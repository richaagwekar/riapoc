package com.csc.rest.dao;

import java.util.List;

import com.csc.rest.model.ResponseInfo;
import com.csc.rest.model.Search;

public interface DataDao {

	
	public List paSearch(Search search) throws Exception;
	
	public List GetPolicyDetails(ResponseInfo responseInfo) throws Exception;
}
