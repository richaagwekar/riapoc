package com.csc.rest.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.csc.rest.model.Asb5cpl1TO;
import com.csc.rest.model.Asbbcpl1TO;
import com.csc.rest.model.Asbjcpl1TO;
import com.csc.rest.model.Asbkcpl1TO;
import com.csc.rest.model.Asblcpl1TO;
import com.csc.rest.model.Asbucpl1TO;
import com.csc.rest.model.Asbycpl1TO;
import com.csc.rest.model.Pmsp0200TO;
import com.csc.rest.model.ResponseInfo;
import com.csc.rest.model.Search;

public class DataDaoImpl implements DataDao {

	@Autowired
	SessionFactory sessionFactory ;

	Session session = null;
	Transaction tx = null;
	int loc, unit, cov;
	static final Logger logger = Logger.getLogger(DataDaoImpl.class);

	@SuppressWarnings("rawtypes")
	@Override
	public List paSearch(Search search) throws Exception {

		logger.debug("Extracting Resultset...");
		int size = search.searchInfo.searchCriteriaInfo
				.getSearchCriteriaValue();
		String dt = search.searchInfo.partialPolicy.getContractTerm();
		session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Pmsp0200TO.class);
		cr.setMaxResults(size);

		cr.add(Restrictions.eq("SYMBOL",
				search.searchInfo.partialPolicy.getCompanyProductCd()));
		cr.add(Restrictions.eq("MASTER0CO",
				search.searchInfo.partialPolicy.getNAICCd()));
		if(dt!=null)
		{
		cr.add(Restrictions.eq("EFF0YR", "1" + dt.substring(2, 4)));
		cr.add(Restrictions.eq("EFF0MO", dt.substring(5, 7)));
		cr.add(Restrictions.eq("EFF0DA", dt.substring(8, 10)));
		}

		@SuppressWarnings("unchecked")
		List resultList = (List<Pmsp0200TO>) cr.list();
		logger.debug("Returning resultset....");
		return resultList;

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List GetPolicyDetails(ResponseInfo responseInfo) throws Exception {

		List Resultset = new ArrayList<List>();
		logger.debug("Extracting Resultset...");
		String dt = responseInfo.persPolicy.contractTerm.getEffectiveDt();
		session = sessionFactory.openSession();
		Criteria cr = session.createCriteria(Pmsp0200TO.class);

		cr.add(Restrictions.eq("POLICY0NUM",
				responseInfo.persPolicy.getPolicynum()));
		cr.add(Restrictions.eq("EFF0YR", "1" + dt.substring(2, 4)));
		cr.add(Restrictions.eq("EFF0MO", dt.substring(5, 7)));
		cr.add(Restrictions.eq("EFF0DA", dt.substring(8, 10)));
		cr.add(Restrictions.eq("SYMBOL", responseInfo.persPolicy.getSymbol()));
		cr.add(Restrictions.eq("MODULE", responseInfo.persPolicy.getModule()));
		cr.add(Restrictions.eq("MASTER0CO",
				responseInfo.persPolicy.getMasterco()));
		cr.add(Restrictions.eq("LOCATION",
				responseInfo.persPolicy.getLocation()));

		List resultList = (List<Pmsp0200TO>) cr.list();

		logger.debug("Returning resultset....");
		Resultset.add(resultList);

		Criteria cr1 = session.createCriteria(Asbucpl1TO.class);
		cr1.add(Restrictions.eq("BUAACD", responseInfo.persPolicy.getModule()));
		cr1.add(Restrictions.eq("BUABCD", responseInfo.persPolicy.getMasterco()));
		cr1.add(Restrictions.eq("BUARTX", responseInfo.persPolicy.getSymbol()));
		cr1.add(Restrictions.eq("BUADNB", responseInfo.persPolicy.getModule()));
		cr1.add(Restrictions.eq("BUASTX",
				responseInfo.persPolicy.getPolicynum()));
		List resultList1 = (List<Asbucpl1TO>) cr1.list();
		loc = resultList1.size();
		Resultset.add(resultList1);

		if (responseInfo.persPolicy.getSymbol().equals("APV")) {
			Criteria cr2 = session.createCriteria(Asblcpl1TO.class);
			cr2.add(Restrictions.eq("BLAACD",
					responseInfo.persPolicy.getModule()));
			cr2.add(Restrictions.eq("BLABCD",
					responseInfo.persPolicy.getMasterco()));
			cr2.add(Restrictions.eq("BLARTX",
					responseInfo.persPolicy.getSymbol()));
			cr2.add(Restrictions.eq("BLADNB",
					responseInfo.persPolicy.getModule()));
			cr2.add(Restrictions.eq("BLASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList2 = (List<Asblcpl1TO>) cr2.list();
			Resultset.add(resultList2);

			Criteria cr3 = session.createCriteria(Asbjcpl1TO.class);
			cr3.add(Restrictions.eq("BJAACD",
					responseInfo.persPolicy.getModule()));
			cr3.add(Restrictions.eq("BJABCD",
					responseInfo.persPolicy.getMasterco()));
			cr3.add(Restrictions.eq("BJARTX",
					responseInfo.persPolicy.getSymbol()));
			cr3.add(Restrictions.eq("BJADNB",
					responseInfo.persPolicy.getModule()));
			cr3.add(Restrictions.eq("BJASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList3 = (List<Asbjcpl1TO>) cr3.list();
			Resultset.add(resultList3);

			Criteria cr4 = session.createCriteria(Asbkcpl1TO.class);
			cr4.add(Restrictions.eq("BKAACD",
					responseInfo.persPolicy.getModule()));
			cr4.add(Restrictions.eq("BKABCD",
					responseInfo.persPolicy.getMasterco()));
			cr4.add(Restrictions.eq("BKARTX",
					responseInfo.persPolicy.getSymbol()));
			cr4.add(Restrictions.eq("BKADNB",
					responseInfo.persPolicy.getModule()));
			cr4.add(Restrictions.eq("BKASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList4 = (List<Asbkcpl1TO>) cr4.list();
			Resultset.add(resultList4);
		} 
		else if (responseInfo.persPolicy.getSymbol().equals("CPP"))
		
			{
			Criteria cr2 = session.createCriteria(Asbbcpl1TO.class);
			cr2.add(Restrictions.eq("BBAACD",
					responseInfo.persPolicy.getModule()));
			cr2.add(Restrictions.eq("BBABCD",
					responseInfo.persPolicy.getMasterco()));
			cr2.add(Restrictions.eq("BBARTX",
					responseInfo.persPolicy.getSymbol()));
			cr2.add(Restrictions.eq("BBADNB",
					responseInfo.persPolicy.getModule()));
			cr2.add(Restrictions.eq("BBASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList2 = (List<Asblcpl1TO>) cr2.list();
			Resultset.add(resultList2);
			
			Criteria cr3 = session.createCriteria(Asb5cpl1TO.class);
			cr3.add(Restrictions.eq("B5AACD",
					responseInfo.persPolicy.getModule()));
			cr3.add(Restrictions.eq("B5ABCD",
					responseInfo.persPolicy.getMasterco()));
			cr3.add(Restrictions.eq("B5ARTX",
					responseInfo.persPolicy.getSymbol()));
			cr3.add(Restrictions.eq("B5ADNB",
					responseInfo.persPolicy.getModule()));
			cr3.add(Restrictions.eq("B5ASTX",
					responseInfo.persPolicy.getPolicynum()));
			
			List resultList3 = (List<Asbkcpl1TO>) cr3.list();
			Resultset.add(resultList3);
			
			Criteria cr4 = session.createCriteria(Asbycpl1TO.class);
			cr4.add(Restrictions.eq("BYAACD",
					responseInfo.persPolicy.getModule()));
			cr4.add(Restrictions.eq("BYABCD",
					responseInfo.persPolicy.getMasterco()));
			cr4.add(Restrictions.eq("BYARTX",
					responseInfo.persPolicy.getSymbol()));
			cr4.add(Restrictions.eq("BYADNB",
					responseInfo.persPolicy.getModule()));
			cr4.add(Restrictions.eq("BYASTX",
					responseInfo.persPolicy.getPolicynum()));
			List resultList4 = (List<Asbjcpl1TO>) cr4.list();
			Resultset.add(resultList4);
			
			
			}
		return Resultset;

	}
}
