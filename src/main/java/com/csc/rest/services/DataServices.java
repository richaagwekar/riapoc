package com.csc.rest.services;

import java.util.List;

import com.csc.rest.model.CommlLine.CommPolicy;
import com.csc.rest.model.PolicyDetails;
import com.csc.rest.model.ResponseInfo;
import com.csc.rest.model.Search;

public interface DataServices {

	public List<ResponseInfo> pASearch(Search search) throws Exception;
	public List<PolicyDetails> GetPolicyDetails(ResponseInfo responseInfo) throws Exception;
	public List<CommPolicy> GetCommlPolicyDetails(ResponseInfo responseInfo) throws Exception;
}
