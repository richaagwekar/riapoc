package com.csc.rest.services;

import java.text.Format;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.csc.rest.dao.*;

import com.csc.rest.model.*;

import com.csc.rest.model.CommlLine.*;
import com.csc.rest.model.CommlLine.IMCPropertyInfo.CommlCov;
import com.csc.rest.model.Asb5cpl1TO;
import com.csc.rest.model.Asblcpl1TO;

import com.csc.rest.model.Asbbcpl1TO;
import com.csc.rest.model.Asbycpl1TO;
import com.csc.rest.model.CommlLineBusiness.CommlInfo;
import com.csc.rest.model.PersAutoLineBusiness.PersDriver;
import com.csc.rest.model.PersAutoLineBusiness.PersVeh;
import com.csc.rest.model.PolicyDetails.Location;

import com.csc.rest.util.CommonMethod;

public class DataServicesImpl implements DataServices {

	@Autowired
	DataDao dataDao;

	static final Logger logger = Logger.getLogger(DataDaoImpl.class);

	@Override
	public List<ResponseInfo> pASearch(Search search) throws Exception {
		List<ResponseInfo> persPolicyList = new ArrayList<ResponseInfo>();

		List ResultList = dataDao.paSearch(search);

		Pmsp0200TO pmsp0200TO = new Pmsp0200TO();

		int size = search.searchInfo.searchCriteriaInfo
				.getSearchCriteriaValue();

		if (ResultList.isEmpty()) {
			ResponseInfo responseInfo1 = new ResponseInfo();
			responseInfo1.persPolicy.setPolicynum("No such Policy Exists");
			persPolicyList.add(responseInfo1);
			return persPolicyList;
		}
		for (Object rowResult : ResultList) {
			// Create persPolicy object and add to list
			pmsp0200TO = (Pmsp0200TO) rowResult;
			ResponseInfo responseInfo = new ResponseInfo();
			responseInfo.persPolicy.contractTerm.setExpirationDt(CommonMethod
					.FormatDate(pmsp0200TO.getEXP0YR() + pmsp0200TO.getEXP0MO()
							+ pmsp0200TO.getEXP0DA()));
			responseInfo.persPolicy.contractTerm
					.setEffectiveDt(search.searchInfo.partialPolicy
							.getContractTerm());
			responseInfo.persPolicy.setPolicynum(pmsp0200TO.getPOLICY0NUM());
			responseInfo.persPolicy.setSymbol(pmsp0200TO.getSYMBOL());
			responseInfo.persPolicy.setModule(pmsp0200TO.getMODULE());
			responseInfo.persPolicy.setMasterco(pmsp0200TO.getMASTER0CO());
			responseInfo.persPolicy.setLocation(pmsp0200TO.getLOCATION());
			persPolicyList.add(responseInfo);
		}

		return persPolicyList;

	}

	@Override
	public List<PolicyDetails> GetPolicyDetails(ResponseInfo responseInfo)
			throws Exception {
		List<PolicyDetails> policyDetailsList = new ArrayList<PolicyDetails>();
		List<CommPolicy> commlPolicyList = new ArrayList<CommPolicy>();
		List ResultList = dataDao.GetPolicyDetails(responseInfo);
		Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
		Asbucpl1TO asbucpl1TO = new Asbucpl1TO();
		Asblcpl1TO asblcpl1TO = new Asblcpl1TO();
		Asbjcpl1TO asbjcpl1TO = new Asbjcpl1TO();
		Asbkcpl1TO asbkcpl1TO = new Asbkcpl1TO();
		List row = (List) ResultList.get(0);
		int index = 0;
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"spring-config.xml");
		if (ResultList.isEmpty()) {
			PolicyDetails pd = new PolicyDetails();
			pd.PersPolicy.setPolicyNumber("No such Policy Exists");
			policyDetailsList.add(pd);
			return policyDetailsList;
		}
		if (responseInfo.persPolicy.getSymbol().equals("APV")) {
			PolicyDetails policyDetails = (PolicyDetails) context
					.getBean("policyDetails");

			for (int i = 0; i < ResultList.size(); i++) {
				List Result = (List) ResultList.get(i);
				for (Object rowResult : Result) {
					PersVeh persVeh;

					String Tname = Result.toString().substring(20, 28);
					if (Tname.equals("Pmsp0200")) {
						pmsp0200TO = (Pmsp0200TO) rowResult;

						policyDetails.PersPolicy.setPolicyNumber(pmsp0200TO
								.getPOLICY0NUM());
						policyDetails.PersPolicy.setLOBCd(pmsp0200TO
								.getSYMBOL());
						policyDetails.PersPolicy.setNAICCd(pmsp0200TO
								.getMASTER0CO());
						policyDetails.PersPolicy
								.setControllingStateProvCd(CommonMethod
										.ConvertNumericStateCd(pmsp0200TO
												.getRISK0STATE()));
						policyDetails.PersPolicy.contractTerm
								.setEffectiveDt(responseInfo.persPolicy.contractTerm
										.getEffectiveDt());
						policyDetails.PersPolicy
								.setOriginalInceptionDt(responseInfo.persPolicy.contractTerm
										.getEffectiveDt());
						policyDetails.PersPolicy
								.setRateEffectiveDt(responseInfo.persPolicy.contractTerm
										.getEffectiveDt());
						policyDetails.PersPolicy.setPolicyTermCd(pmsp0200TO
								.getINSTAL0TRM());

						policyDetailsList.add(policyDetails);
					} else if (Tname.equals("Asbucpl1")) {
						asbucpl1TO = (Asbucpl1TO) rowResult;
						Location loc = new Location();
						loc.setId(asbucpl1TO.getBUBRNB());
						loc.addr.setPostalCode(asbucpl1TO.getBUAPNB());
						policyDetails.location.add(loc);
					} else if (Tname.equals("Asblcpl1")) {
						asblcpl1TO = (Asblcpl1TO) rowResult;
						PersDriver persDriver = new PersDriver();
						persDriver.setId(asblcpl1TO.getBLBRNB());
						persDriver.driverInfo.personInfo.setGenderCd(asblcpl1TO
								.getBLA7ST());
						persDriver.driverInfo.personInfo.setBirthDt(asblcpl1TO
								.getBLAGDT());
						persDriver.driverInfo.personInfo
								.setMaritalStatusCd(asblcpl1TO.getBLA8ST());
						// policyDetails.persAutoLineBusiness.persDriver.driverInfo.personInfo.setOccupationClassCd(asblcpl1TO.get)
						policyDetails.persAutoLineBusiness.persDriver
								.add(persDriver);
					} else if (Tname.equals("Asbjcpl1")) {

						asbjcpl1TO = (Asbjcpl1TO) rowResult;
						persVeh = new PersVeh();
						persVeh.setLocationRef("L00" + asbjcpl1TO.getBJAENB());
						persVeh.setId("V00" + asbjcpl1TO.getBJAENB());
						persVeh.setModelYear(asbjcpl1TO.getBJAHNB());
						persVeh.setTerritoryCd(asbjcpl1TO.getBJAGNB());
						persVeh.setVehSymbolCd(asbjcpl1TO.getBJAJNB());
						persVeh.setVehTypeCd(asbjcpl1TO.getBJAZTX());
						persVeh.setVehUseCd(asbjcpl1TO.getBJC5ST());
						persVeh.fulltermAmt.setAmt(asbjcpl1TO.getBJAINB());

						policyDetails.persAutoLineBusiness.persVeh.add(persVeh);
						index++;
					} else if (Tname.equals("Asbkcpl1")) {
						asbkcpl1TO = (Asbkcpl1TO) rowResult;
						Limit limit;
						Coverage coverage = new Coverage();
						coverage.setCoverageCd(asbkcpl1TO.getBKAOTX());
						if (asbkcpl1TO.getBKBBTX().contains("/")) {
							limit = new Limit();
							limit.formatCurrencyAmt.setAmt(asbkcpl1TO
									.getBKBBTX().substring(0, 2).concat("000"));
							coverage.limit.add(limit);
							Limit limit1 = new Limit();
							limit1.formatCurrencyAmt.setAmt(asbkcpl1TO
									.getBKBBTX().substring(4, 6).concat("000"));
							coverage.limit.add(limit1);
							// coverage.limit.add(limit);
						} else {
							limit = new Limit();
							limit.formatCurrencyAmt.setAmt(asbkcpl1TO
									.getBKBBTX());
							coverage.limit.add(limit);
							;
						}
						// coverage.limit.formatCurrencyAmt.setAmt(asbkcpl1TO
						// .getBKBBTX());
						coverage.currentTermAmt.setAmt(asbkcpl1TO.getBKA3VA());
						coverage.deductible.formatCurrencyAmt.setAmt(asbkcpl1TO
								.getBKA9NB());
						policyDetails.persAutoLineBusiness.persVeh
								.get(asbkcpl1TO.getBKAENB() - 1).coverage
								.add(coverage);

					}

				}
			}
		}

		return policyDetailsList;
	}

	public List<CommPolicy> GetCommlPolicyDetails(ResponseInfo responseInfo)
			throws Exception {

		List<CommPolicy> commPolicyList = new ArrayList<CommPolicy>();
		List ResultList = dataDao.GetPolicyDetails(responseInfo);
		Pmsp0200TO pmsp0200TO = new Pmsp0200TO();
		Asbucpl1TO asbucpl1TO = new Asbucpl1TO();
		Asb5cpl1TO asb5cpl1TO = new Asb5cpl1TO();
		Asbycpl1TO asbycpl1TO = new Asbycpl1TO();
		Asbbcpl1TO asbbcpl1TO = new Asbbcpl1TO();

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"/spring-config.xml");
		if (ResultList.isEmpty()) {
			CommPolicy cp = new CommPolicy();
			cp.commlPolicy.setPolicyNumber("No such Policy Exists");
			commPolicyList.add(cp);
			return commPolicyList;
		}
		CommPolicy commPolicy = (CommPolicy) context
				.getBean("CommPolicy", "CF");
		for (int i = 0; i < ResultList.size(); i++) {
			List Result = (List) ResultList.get(i);
			for (Object rowResult : Result) {
				String Tname = Result.toString().substring(20, 28);
				if (Tname.equals("Pmsp0200")) {
					pmsp0200TO = (Pmsp0200TO) rowResult;

					commPolicy.commlPolicy.setPolicyNumber(pmsp0200TO
							.getPOLICY0NUM());
					commPolicy.commlPolicy.setLOBCd(pmsp0200TO.getSYMBOL());
					commPolicy.commlPolicy.setNAICCd(pmsp0200TO.getMASTER0CO());
					commPolicy.commlPolicy
							.setControllingStateProvCd(CommonMethod
									.ConvertNumericStateCd(pmsp0200TO
											.getRISK0STATE()));
					commPolicy.commlPolicy.contractTerm
							.setEffectiveDt(responseInfo.persPolicy.contractTerm
									.getEffectiveDt());

					commPolicy.commlPolicy
							.setOriginalInceptionDt(responseInfo.persPolicy.contractTerm
									.getEffectiveDt());
					commPolicy.commlPolicy
							.setRateEffectiveDt(responseInfo.persPolicy.contractTerm
									.getEffectiveDt());
					commPolicy.commlPolicy.setPolicyTermCd(pmsp0200TO
							.getINSTAL0TRM());

					commPolicyList.add(commPolicy);
				} else if (Tname.equals("Asbbcpl1")) {
					asbbcpl1TO = (Asbbcpl1TO) rowResult;
					// commPolicy.inlandMarineLineBusiness.currentTermAmt.setAmt(asbbcpl1TO.getBBA3VA());
					// commPolicy.commlPropertyLineBusiness.cu
					commPolicy.commlPropertyLineBusiness.currentTermAmt
							.setAmt(asbbcpl1TO.getBBA3VA());

				} else if (Tname.equals("Asbucpl1")) {
					asbucpl1TO = (Asbucpl1TO) rowResult;
					Location loc = new Location();
					loc.setId(asbucpl1TO.getBUBRNB());
					loc.addr.setPostalCode(asbucpl1TO.getBUAPNB());
					commPolicy.location.add(loc);

				} else if (Tname.equals("Asb5cpl1")) {

					asb5cpl1TO = (Asb5cpl1TO) rowResult;
					IMCPropertyInfo PropertyInfo = new IMCPropertyInfo();
					PropertyInfo.setTerritoryCd(asb5cpl1TO.getB5AGNB());
					PropertyInfo.setPMACd(asb5cpl1TO.getB5KWTX());
					PropertyInfo.setClassCd(asb5cpl1TO.getB5PTTX());
					PropertyInfo.setClassCdDesc(asb5cpl1TO.getB5KKTX());
					PropertyInfo.setLocationRef("Loc00"
							+ asb5cpl1TO.getB5BRNB());
					PropertyInfo.setSubLocRef("SubLoc00"
							+ asb5cpl1TO.getB5EGNB());

					if (asb5cpl1TO.getB5AGTX().equals("CF")) {
						commPolicy.commlPropertyLineBusiness.CommlPropertyInfo
								.add(PropertyInfo);
					}
				} else if (Tname.equals("Asbycpl1")) {
					asbycpl1TO = (Asbycpl1TO) rowResult;
					CommlCov cov = new IMCPropertyInfo.CommlCov();
					cov.setCoverageCd(asbycpl1TO.getBYAOTX());
					cov.setIterationNumber(asbycpl1TO.getBYC0NB());
					// commPolicy.inlandMarineLineBusiness.CommlIMInfo.CommlIMPropertyInfo.get(0).CommlCoverage.add(cov);
					if (asbycpl1TO.getBYBRNB() != 0) {
						commPolicy.commlPropertyLineBusiness.CommlPropertyInfo
								.get(asbycpl1TO.getBYBRNB() - 1).CommlCoverage
								.add(cov);
					}
				}
			}
		}
		return commPolicyList;
	}

}
