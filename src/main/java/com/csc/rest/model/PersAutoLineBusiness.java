package com.csc.rest.model;

import java.util.ArrayList;
import java.util.List;

public class PersAutoLineBusiness {
	public Coverage coverage = new Coverage();
	public List<PersDriver> persDriver = new ArrayList<PersDriver>();
	public List<PersVeh> persVeh = new ArrayList<PersVeh>();

	public static class PersDriver

	{
		String Id;

		public String getId() {
			return Id;
		}

		public void setId(String id) {
			Id = id;
		}

		public DriverInfo driverInfo = new DriverInfo();

		public class DriverInfo {
			public PersonInfo personInfo = new PersonInfo();

			public class PersonInfo {
				public String GenderCd;
				public String BirthDt;
				String MaritalStatusCd;
				String OccupationClassCd;

				public String getGenderCd() {
					return GenderCd;
				}

				public void setGenderCd(String genderCd) {
					GenderCd = genderCd;
				}

				public String getBirthDt() {
					return BirthDt;
				}

				public void setBirthDt(String birthDt) {
					BirthDt = birthDt;
				}

				public String getMaritalStatusCd() {
					return MaritalStatusCd;
				}

				public void setMaritalStatusCd(String maritalStatusCd) {
					MaritalStatusCd = maritalStatusCd;
				}

				public String getOccupationClassCd() {
					return OccupationClassCd;
				}

				public void setOccupationClassCd(String occupationClassCd) {
					OccupationClassCd = occupationClassCd;
				}

			}
		}
	}

	public static class PersVeh {
		String LocationRef;
		String Id;
		String ModelYear;
		String VehTypeCd;
		public FullTermAmt fulltermAmt = new FullTermAmt();
		String TerritoryCd;
		String VehSymbolCd;
		String VehUseCd;
		public List<Coverage> coverage = new ArrayList<Coverage>();

		public String getLocationRef() {
			return LocationRef;
		}

		public void setLocationRef(String locationRef) {
			LocationRef = locationRef;
		}

		public String getId() {
			return Id;
		}

		public void setId(String id) {
			Id = id;
		}

		public String getModelYear() {
			return ModelYear;
		}

		public void setModelYear(String modelYear) {
			ModelYear = modelYear;
		}

		public String getVehTypeCd() {
			return VehTypeCd;
		}

		public void setVehTypeCd(String vehTypeCd) {
			VehTypeCd = vehTypeCd;
		}

		public FullTermAmt getFulltermAmt() {
			return fulltermAmt;
		}

		public void setFulltermAmt(FullTermAmt fulltermAmt) {
			this.fulltermAmt = fulltermAmt;
		}

		public String getTerritoryCd() {
			return TerritoryCd;
		}

		public void setTerritoryCd(String territoryCd) {
			TerritoryCd = territoryCd;
		}

		public String getVehSymbolCd() {
			return VehSymbolCd;
		}

		public void setVehSymbolCd(String vehSymbolCd) {
			VehSymbolCd = vehSymbolCd;
		}

		public String getVehUseCd() {
			return VehUseCd;
		}

		public void setVehUseCd(String vehUseCd) {
			VehUseCd = vehUseCd;
		}

		public class FullTermAmt {
			String Amt;

			public String getAmt() {
				return Amt;
			}

			public void setAmt(String amt) {
				Amt = amt;
			}
		}
	}

}
