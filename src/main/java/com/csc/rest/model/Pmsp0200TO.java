package com.csc.rest.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@Entity
@Table(name = "PMSP0200")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Pmsp0200TO implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Id
	private String POLICY0NUM="";
	private String SYMBOL="";
	private String MASTER0CO="";
	
	private String AMEND0NUM = "";
	private String EFF0YR = "";
	private String EFF0MO = "";
	private String EFF0DA = "";
	private String EXP0YR = "";
	private String EXP0MO = "";
	private String EXP0DA = "";
	private String INSTAL0TRM = "";
	private String NMB0INSTAL = "";
	private String RISK0STATE = "";
	private String COMPANY0NO = "";
	private String BRANCH = "";
	private String PROFIT0CTR = "";
	private String FILLR1 = "";
	private char RPT0AGT0NR;
	private String FILLR2 = "";
	private String ENTER0DATE = "";
	private BigDecimal TOT0AG0PRM = BigDecimal.ZERO;
	private String LINE0BUS = "";
	private char ISSUE0CODE;
	private char COMP0LINE;
	private char PAY0CODE;
	private char MODE0CODE;
	private char AUDIT0CODE;
	private char KIND0CODE;
	private char VARIATION;
	private String SORT0NAME = "";
	private String PROD0CODE = "";
	private char REVIEW0CD;
	private char MVR0RPT0YR;
	private char RISK0GRADE;
	private char RISK0UNDWR;
	private char RENEWAL0CD;
	private char RSN0AM01ST;
	private char RSN0AM02ND;
	private char RSN0AM03RD;
	private char RENEW0PAY;
	private char RENEW0MODE;
	private String RN0POL0SYM = "";
	private String RN0POL0NUM = "";
	private int ORI0INCEPT;
	private String CUST0NO = "";
	private String SPEC0USE0A = "";
	private String SPEC0USE0B = "";
	private String ZIP0POST = "";
	private String ADD0LINE01 = "";
	private char FILLR3;
	private String FILLR4 = "";
	private String ADD0LINE03 = "";
	private String ADD0LINE04 = "";
	private char HIST0OPT;
	private String FIN0AUD0IN = "";
	private char EXC0CLAIM;
	private String FILLR5 = "";
	private String TYPE0ACT = "";
	private String CANCELDATE = "";
	private String MRSSEQ = "";
	private String MRSUID = "";
	private String MODULE="";
	private String LOCATION="";
	
	public String getLOCATION() {
		return LOCATION;
	}
	public void setLOCATION(String lOCATION) {
		LOCATION = lOCATION;
	}
	public String getMODULE() {
		return MODULE;
	}
	public void setMODULE(String mODULE) {
		MODULE = mODULE;
	}
	public String getAMEND0NUM() {
		return AMEND0NUM;
	}
	public void setAMEND0NUM(String aMEND0NUM) {
		AMEND0NUM = aMEND0NUM;
	}
	public String getEFF0YR() {
		return EFF0YR;
	}
	public void setEFF0YR(String eFF0YR) {
		EFF0YR = eFF0YR;
	}
	public String getEFF0MO() {
		return EFF0MO;
	}
	public void setEFF0MO(String eFF0MO) {
		EFF0MO = eFF0MO;
	}
	public String getEFF0DA() {
		return EFF0DA;
	}
	public void setEFF0DA(String eFF0DA) {
		EFF0DA = eFF0DA;
	}
	public String getEXP0YR() {
		return EXP0YR;
	}
	public void setEXP0YR(String eXP0YR) {
		EXP0YR = eXP0YR;
	}
	public String getEXP0MO() {
		return EXP0MO;
	}
	public void setEXP0MO(String eXP0MO) {
		EXP0MO = eXP0MO;
	}
	public String getEXP0DA() {
		return EXP0DA;
	}
	public void setEXP0DA(String eXP0DA) {
		EXP0DA = eXP0DA;
	}
	public String getINSTAL0TRM() {
		return INSTAL0TRM;
	}
	public void setINSTAL0TRM(String iNSTAL0TRM) {
		INSTAL0TRM = iNSTAL0TRM;
	}
	public String getNMB0INSTAL() {
		return NMB0INSTAL;
	}
	public void setNMB0INSTAL(String nMB0INSTAL) {
		NMB0INSTAL = nMB0INSTAL;
	}
	public String getRISK0STATE() {
		return RISK0STATE;
	}
	public void setRISK0STATE(String rISK0STATE) {
		RISK0STATE = rISK0STATE;
	}
	public String getCOMPANY0NO() {
		return COMPANY0NO;
	}
	public void setCOMPANY0NO(String cOMPANY0NO) {
		COMPANY0NO = cOMPANY0NO;
	}
	public String getBRANCH() {
		return BRANCH;
	}
	public void setBRANCH(String bRANCH) {
		BRANCH = bRANCH;
	}
	public String getPROFIT0CTR() {
		return PROFIT0CTR;
	}
	public void setPROFIT0CTR(String pROFIT0CTR) {
		PROFIT0CTR = pROFIT0CTR;
	}
	public String getFILLR1() {
		return FILLR1;
	}
	public void setFILLR1(String fILLR1) {
		FILLR1 = fILLR1;
	}
	public char getRPT0AGT0NR() {
		return RPT0AGT0NR;
	}
	public void setRPT0AGT0NR(char rPT0AGT0NR) {
		RPT0AGT0NR = rPT0AGT0NR;
	}
	public String getFILLR2() {
		return FILLR2;
	}
	public void setFILLR2(String fILLR2) {
		FILLR2 = fILLR2;
	}
	public String getENTER0DATE() {
		return ENTER0DATE;
	}
	public void setENTER0DATE(String eNTER0DATE) {
		ENTER0DATE = eNTER0DATE;
	}
	public BigDecimal getTOT0AG0PRM() {
		return TOT0AG0PRM;
	}
	public void setTOT0AG0PRM(BigDecimal tOT0AG0PRM) {
		TOT0AG0PRM = tOT0AG0PRM;
	}
	public String getLINE0BUS() {
		return LINE0BUS;
	}
	public void setLINE0BUS(String lINE0BUS) {
		LINE0BUS = lINE0BUS;
	}
	public char getISSUE0CODE() {
		return ISSUE0CODE;
	}
	public void setISSUE0CODE(char iSSUE0CODE) {
		ISSUE0CODE = iSSUE0CODE;
	}
	public char getCOMP0LINE() {
		return COMP0LINE;
	}
	public void setCOMP0LINE(char cOMP0LINE) {
		COMP0LINE = cOMP0LINE;
	}
	public char getPAY0CODE() {
		return PAY0CODE;
	}
	public void setPAY0CODE(char pAY0CODE) {
		PAY0CODE = pAY0CODE;
	}
	public char getMODE0CODE() {
		return MODE0CODE;
	}
	public void setMODE0CODE(char mODE0CODE) {
		MODE0CODE = mODE0CODE;
	}
	public char getAUDIT0CODE() {
		return AUDIT0CODE;
	}
	public void setAUDIT0CODE(char aUDIT0CODE) {
		AUDIT0CODE = aUDIT0CODE;
	}
	public char getKIND0CODE() {
		return KIND0CODE;
	}
	public void setKIND0CODE(char kIND0CODE) {
		KIND0CODE = kIND0CODE;
	}
	public char getVARIATION() {
		return VARIATION;
	}
	public void setVARIATION(char vARIATION) {
		VARIATION = vARIATION;
	}
	public String getSORT0NAME() {
		return SORT0NAME;
	}
	public void setSORT0NAME(String sORT0NAME) {
		SORT0NAME = sORT0NAME;
	}
	public String getPROD0CODE() {
		return PROD0CODE;
	}
	public void setPROD0CODE(String pROD0CODE) {
		PROD0CODE = pROD0CODE;
	}
	public char getREVIEW0CD() {
		return REVIEW0CD;
	}
	public void setREVIEW0CD(char rEVIEW0CD) {
		REVIEW0CD = rEVIEW0CD;
	}
	public char getMVR0RPT0YR() {
		return MVR0RPT0YR;
	}
	public void setMVR0RPT0YR(char mVR0RPT0YR) {
		MVR0RPT0YR = mVR0RPT0YR;
	}
	public char getRISK0GRADE() {
		return RISK0GRADE;
	}
	public void setRISK0GRADE(char rISK0GRADE) {
		RISK0GRADE = rISK0GRADE;
	}
	public char getRISK0UNDWR() {
		return RISK0UNDWR;
	}
	public void setRISK0UNDWR(char rISK0UNDWR) {
		RISK0UNDWR = rISK0UNDWR;
	}
	public char getRENEWAL0CD() {
		return RENEWAL0CD;
	}
	public void setRENEWAL0CD(char rENEWAL0CD) {
		RENEWAL0CD = rENEWAL0CD;
	}
	public char getRSN0AM01ST() {
		return RSN0AM01ST;
	}
	public void setRSN0AM01ST(char rSN0AM01ST) {
		RSN0AM01ST = rSN0AM01ST;
	}
	public char getRSN0AM02ND() {
		return RSN0AM02ND;
	}
	public void setRSN0AM02ND(char rSN0AM02ND) {
		RSN0AM02ND = rSN0AM02ND;
	}
	public char getRSN0AM03RD() {
		return RSN0AM03RD;
	}
	public void setRSN0AM03RD(char rSN0AM03RD) {
		RSN0AM03RD = rSN0AM03RD;
	}
	public char getRENEW0PAY() {
		return RENEW0PAY;
	}
	public void setRENEW0PAY(char rENEW0PAY) {
		RENEW0PAY = rENEW0PAY;
	}
	public char getRENEW0MODE() {
		return RENEW0MODE;
	}
	public void setRENEW0MODE(char rENEW0MODE) {
		RENEW0MODE = rENEW0MODE;
	}
	public String getRN0POL0SYM() {
		return RN0POL0SYM;
	}
	public void setRN0POL0SYM(String rN0POL0SYM) {
		RN0POL0SYM = rN0POL0SYM;
	}
	public String getRN0POL0NUM() {
		return RN0POL0NUM;
	}
	public void setRN0POL0NUM(String rN0POL0NUM) {
		RN0POL0NUM = rN0POL0NUM;
	}
	public int getORI0INCEPT() {
		return ORI0INCEPT;
	}
	public void setORI0INCEPT(int oRI0INCEPT) {
		ORI0INCEPT = oRI0INCEPT;
	}
	public String getCUST0NO() {
		return CUST0NO;
	}
	public void setCUST0NO(String cUST0NO) {
		CUST0NO = cUST0NO;
	}
	public String getSPEC0USE0A() {
		return SPEC0USE0A;
	}
	public void setSPEC0USE0A(String sPEC0USE0A) {
		SPEC0USE0A = sPEC0USE0A;
	}
	public String getSPEC0USE0B() {
		return SPEC0USE0B;
	}
	public void setSPEC0USE0B(String sPEC0USE0B) {
		SPEC0USE0B = sPEC0USE0B;
	}
	public String getZIP0POST() {
		return ZIP0POST;
	}
	public void setZIP0POST(String zIP0POST) {
		ZIP0POST = zIP0POST;
	}
	public String getADD0LINE01() {
		return ADD0LINE01;
	}
	public void setADD0LINE01(String aDD0LINE01) {
		ADD0LINE01 = aDD0LINE01;
	}
	public char getFILLR3() {
		return FILLR3;
	}
	public void setFILLR3(char fILLR3) {
		FILLR3 = fILLR3;
	}
	public String getFILLR4() {
		return FILLR4;
	}
	public void setFILLR4(String fILLR4) {
		FILLR4 = fILLR4;
	}
	public String getADD0LINE03() {
		return ADD0LINE03;
	}
	public void setADD0LINE03(String aDD0LINE03) {
		ADD0LINE03 = aDD0LINE03;
	}
	public String getADD0LINE04() {
		return ADD0LINE04;
	}
	public void setADD0LINE04(String aDD0LINE04) {
		ADD0LINE04 = aDD0LINE04;
	}
	public char getHIST0OPT() {
		return HIST0OPT;
	}
	public void setHIST0OPT(char hIST0OPT) {
		HIST0OPT = hIST0OPT;
	}
	public String getFIN0AUD0IN() {
		return FIN0AUD0IN;
	}
	public void setFIN0AUD0IN(String fIN0AUD0IN) {
		FIN0AUD0IN = fIN0AUD0IN;
	}
	public char getEXC0CLAIM() {
		return EXC0CLAIM;
	}
	public void setEXC0CLAIM(char eXC0CLAIM) {
		EXC0CLAIM = eXC0CLAIM;
	}
	public String getFILLR5() {
		return FILLR5;
	}
	public void setFILLR5(String fILLR5) {
		FILLR5 = fILLR5;
	}
	public String getTYPE0ACT() {
		return TYPE0ACT;
	}
	public void setTYPE0ACT(String tYPE0ACT) {
		TYPE0ACT = tYPE0ACT;
	}
	public String getCANCELDATE() {
		return CANCELDATE;
	}
	public void setCANCELDATE(String cANCELDATE) {
		CANCELDATE = cANCELDATE;
	}
	public String getMRSSEQ() {
		return MRSSEQ;
	}
	public void setMRSSEQ(String mRSSEQ) {
		MRSSEQ = mRSSEQ;
	}
	public String getMRSUID() {
		return MRSUID;
	}
	public void setMRSUID(String mRSUID) {
		MRSUID = mRSUID;
	}
	public String getMASTER0CO() {
		return MASTER0CO;
	}
	public void setMASTER0CO(String mASTER0CO) {
		MASTER0CO = mASTER0CO;
	}
	public String getSYMBOL() {
		return SYMBOL;
	}
	public void setSYMBOL(String sYMBOL) {
		SYMBOL = sYMBOL;
	}

	public String getPOLICY0NUM() {
		return POLICY0NUM;
	}
	public void setPOLICY0NUM(String pOLICY0NUM) {
		POLICY0NUM = pOLICY0NUM;
	}
	

}
