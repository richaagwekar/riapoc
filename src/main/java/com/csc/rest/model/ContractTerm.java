package com.csc.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;



@XmlAccessorType(XmlAccessType.NONE)
public class ContractTerm {
	@XmlElement
	public String EffectiveDt = "";

	public String getEffectiveDt() {
		return EffectiveDt;
	}
	@XmlElement
	public String ExpirationDt = "";

	public void setEffectiveDt(String effectiveDt) {
		EffectiveDt = effectiveDt;
	}

	public void setExpirationDt(String expirationDt) {
		ExpirationDt = expirationDt;
	}

	public String getExpirationDt() {
		return EffectiveDt;
	}

}
