package com.csc.rest.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ASBBCPL1")
public class Asbbcpl1TO implements Serializable

{
	private static final long serialVersionUID = 1L;

	@Id
	private String BBASTX="";
	@Id
	private String BBAGTX="";
	@Id
	private String BBADNB="";
	private String BBA3VA="";
	private String BBARTX="";
	private String BBAACD="";
	private String BBABCD="";
	public String getBBASTX() {
		return BBASTX;
	}
	public void setBBASTX(String bBASTX) {
		BBASTX = bBASTX;
	}
	public String getBBAGTX() {
		return BBAGTX;
	}
	public void setBBAGTX(String bBAGTX) {
		BBAGTX = bBAGTX;
	}
	public String getBBADNB() {
		return BBADNB;
	}
	public void setBBADNB(String bBADNB) {
		BBADNB = bBADNB;
	}
	public String getBBA3VA() {
		return BBA3VA;
	}
	public void setBBA3VA(String bBA3VA) {
		BBA3VA = bBA3VA;
	}
	public String getBBARTX() {
		return BBARTX;
	}
	public void setBBARTX(String bBARTX) {
		BBARTX = bBARTX;
	}
	public String getBBAACD() {
		return BBAACD;
	}
	public void setBBAACD(String bBAACD) {
		BBAACD = bBAACD;
	}
	public String getBBABCD() {
		return BBABCD;
	}
	public void setBBABCD(String bBABCD) {
		BBABCD = bBABCD;
	}




	
}
