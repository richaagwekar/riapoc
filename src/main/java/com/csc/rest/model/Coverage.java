package com.csc.rest.model;

import java.util.ArrayList;
import java.util.List;

public class Coverage 
{
	String CoverageCd;
	public List<Limit> limit=new ArrayList<Limit>();
	
	public String getCoverageCd()
	{
		return CoverageCd;
	}
	public void setCoverageCd(String coverageCd)
	{
		CoverageCd=coverageCd;
	}
	
	public CurrentTermAmt currentTermAmt=new CurrentTermAmt();
	public Deductible deductible=new Deductible();
	
	public class CurrentTermAmt
	{
		String Amt;

		public String getAmt() {
			return Amt;
		}

		public void setAmt(String amt) {
			Amt = amt;
		}
	}
	
	

}
