package com.csc.rest.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlAccessorType(XmlAccessType.NONE)
public class SearchInfo implements Serializable {
@XmlElement
	public PartialPolicy partialPolicy = new PartialPolicy();
@XmlElement
	public SearchCriteriaInfo searchCriteriaInfo = new SearchCriteriaInfo();

}