package com.csc.rest.model;

public class Limit
{
	public FormatCurrencyAmt formatCurrencyAmt=new FormatCurrencyAmt();
	public class FormatCurrencyAmt
	{
		String Amt;

		public String getAmt() {
			return Amt;
		}

		public void setAmt(String amt) {
			Amt = amt;
		}
	}
}