package com.csc.rest.model;

public class CommercialLineBusiness extends LineOfBusiness{
	String lob;

	public String getLob() {
		return lob;
	}

	public void setLob(String lob) {
		this.lob = lob;
	}

}
