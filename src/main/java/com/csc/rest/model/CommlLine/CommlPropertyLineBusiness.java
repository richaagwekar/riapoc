package com.csc.rest.model.CommlLine;

import java.util.ArrayList;
import java.util.List;

import com.csc.rest.model.CommlLine.InlandMarineLineBusiness.CurrentTermAmt;

public class CommlPropertyLineBusiness 
{
	String LOBCd="CF";
	public CurrentTermAmt currentTermAmt=new InlandMarineLineBusiness.CurrentTermAmt();
	public List<IMCPropertyInfo> CommlPropertyInfo=new ArrayList<IMCPropertyInfo>();
	public String getLOBCd() {
		return LOBCd;
	}
	public void setLOBCd(String lOBCd) {
		LOBCd = lOBCd;
	}
	
	
}
