package com.csc.rest.model.CommlLine;

import java.util.ArrayList;
import java.util.List;

public class InlandMarineLineBusiness
{
	String LOBCd="IMC";
    public CurrentTermAmt currentTermAmt=new CurrentTermAmt();
    public InlandMarineInfo CommlIMInfo=new InlandMarineInfo();
    public String getLOBCd() {
		return LOBCd;
	}

	public void setLOBCd(String lOBCd) {
		LOBCd = lOBCd;
	}
	public static class CurrentTermAmt

	{
		String Amt;

		public String getAmt() {
			return Amt;
		}

		public void setAmt(String amt) {
			Amt = amt;
		}
	}
	public class InlandMarineInfo
	{
		public List<IMCPropertyInfo> CommlIMPropertyInfo=new ArrayList<IMCPropertyInfo>();
	}
	
	
}
