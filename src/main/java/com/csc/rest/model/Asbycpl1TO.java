package com.csc.rest.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ASBYCPL1")
public class Asbycpl1TO implements Serializable
{
private static final long serialVersionUID = 1L;
	
	@Id
	private String BYASTX="";
	@Id
	private int BYBRNB;
	@Id
	private String BYAGTX="";
	@Id
	
	private String BYARTX="";
	private String BYAACD="";
	private String BYABCD="";
	private String BYANTX="";
	
	private String BYADNB="";
	private String BYAGNB="";
	private String BYC0NB="";
	public String getBYC0NB() {
		return BYC0NB;
	}
	public void setBYC0NB(String bYC0NB) {
		BYC0NB = bYC0NB;
	}
	@Id
	private String BYAOTX="";
	
	
	public String getBYAOTX() {
		return BYAOTX;
	}
	public void setBYAOTX(String bYAOTX) {
		BYAOTX = bYAOTX;
	}
	public String getBYASTX() {
		return BYASTX;
	}
	public void setBYASTX(String bYASTX) {
		BYASTX = bYASTX;
	}
	public String getBYAGTX() {
		return BYAGTX;
	}
	public void setBYAGTX(String bYAGTX) {
		BYAGTX = bYAGTX;
	}
	public String getBYARTX() {
		return BYARTX;
	}
	public void setBYARTX(String bYARTX) {
		BYARTX = bYARTX;
	}
	public String getBYAACD() {
		return BYAACD;
	}
	public void setBYAACD(String bYAACD) {
		BYAACD = bYAACD;
	}
	public String getBYABCD() {
		return BYABCD;
	}
	public void setBYABCD(String bYABCD) {
		BYABCD = bYABCD;
	}
	public String getBYANTX() {
		return BYANTX;
	}
	public void setBYANTX(String bYANTX) {
		BYANTX = bYANTX;
	}
	public int getBYBRNB() {
		return BYBRNB;
	}
	public void setBYBRNB(int bYBRNB) {
		BYBRNB = bYBRNB;
	}
	public String getBYADNB() {
		return BYADNB;
	}
	public void setBYADNB(String bYADNB) {
		BYADNB = bYADNB;
	}
	public String getBYAGNB() {
		return BYAGNB;
	}
	public void setBYAGNB(String bYAGNB) {
		BYAGNB = bYAGNB;
	}
	
	
}
