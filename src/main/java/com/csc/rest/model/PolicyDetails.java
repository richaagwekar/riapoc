package com.csc.rest.model;

import java.util.ArrayList;
import java.util.List;

public class PolicyDetails {

	public PersPolicy PersPolicy;
	public List<Location> location = new ArrayList<Location>();
	public PersAutoLineBusiness persAutoLineBusiness;

	public void setPersPolicy(PersPolicy persPolicy) {
		PersPolicy = persPolicy;
	}

	public PersAutoLineBusiness getPersAutoLineBusiness() {
		return persAutoLineBusiness;
	}

	public void setPersAutoLineBusiness(
			PersAutoLineBusiness persAutoLineBusiness) {
		this.persAutoLineBusiness = persAutoLineBusiness;
	}

	public static class PersPolicy {

		String PolicyNumber;
		String LOBCd;
		String NAICCd;
		String ControllingStateProvCd;
		public ContractTerm contractTerm;

		String CurrentTermAmt;
		String OriginalInceptionDt;
		String RateEffectiveDt;
		public CreditScoreInfo creditScoreInfo;
		String PolicyTermCd;
		PersApplicationInfo persApplicationInfo;

		public ContractTerm getContractTerm() {
			return contractTerm;
		}

		public void setContractTerm(ContractTerm contractTerm) {
			this.contractTerm = contractTerm;
		}

		public String getCurrentTermAmt() {
			return CurrentTermAmt;
		}

		public void setCurrentTermAmt(String currentTermAmt) {
			CurrentTermAmt = currentTermAmt;
		}

		public String getOriginalInceptionDt() {
			return OriginalInceptionDt;
		}

		public void setOriginalInceptionDt(String originalInceptionDt) {
			OriginalInceptionDt = originalInceptionDt;
		}

		public String getRateEffectiveDt() {
			return RateEffectiveDt;
		}

		public void setRateEffectiveDt(String rateEffectiveDt) {
			RateEffectiveDt = rateEffectiveDt;
		}

		public String getPolicyTermCd() {
			return PolicyTermCd;
		}

		public void setPolicyTermCd(String policyTermCd) {
			PolicyTermCd = policyTermCd;
		}

		public PersApplicationInfo getPersApplicationInfo() {
			return persApplicationInfo;
		}

		public void setPersApplicationInfo(
				PersApplicationInfo persApplicationInfo) {
			this.persApplicationInfo = persApplicationInfo;
		}

		public String getPolicyNumber() {
			return PolicyNumber;
		}

		public void setPolicyNumber(String policyNumber) {
			PolicyNumber = policyNumber;
		}

		public String getControllingStateProvCd() {
			return ControllingStateProvCd;
		}

		public void setControllingStateProvCd(String controllingStateProvCd) {
			ControllingStateProvCd = controllingStateProvCd;
		}

		public CreditScoreInfo getCreditScoreInfo() {
			return creditScoreInfo;
		}

		public void setCreditScoreInfo(CreditScoreInfo creditScoreInfo) {
			this.creditScoreInfo = creditScoreInfo;
		}

		public String getLOBCd() {
			return LOBCd;
		}

		public void setLOBCd(String lOBCd) {
			LOBCd = lOBCd;
		}

		public String getNAICCd() {
			return NAICCd;
		}

		public void setNAICCd(String nAICCd) {
			NAICCd = nAICCd;
		}

	}

	public static class Location {

		public Addr addr = new Addr();;
		String Id;

		public String getId() {
			return Id;
		}

		public void setId(String id) {
			Id = id;
		}

		public static class Addr {
			String PostalCode;

			public String getPostalCode() {
				return PostalCode;
			}

			public void setPostalCode(String postalCode) {
				PostalCode = postalCode;
			}
		}
	}

	public static class CreditScoreInfo {
		public String getCreditScore() {
			return CreditScore;
		}

		public void setCreditScore(String creditScore) {
			CreditScore = creditScore;
		}

		String CreditScore;
	}

	public static class PersApplicationInfo {
		public String getApplicationReceivedByAgencyDt() {
			return ApplicationReceivedByAgencyDt;
		}

		public void setApplicationReceivedByAgencyDt(
				String applicationReceivedByAgencyDt) {
			ApplicationReceivedByAgencyDt = applicationReceivedByAgencyDt;
		}

		String ApplicationReceivedByAgencyDt;
	}

	public static class ContractTerm {
		String EffectiveDt;

		public String getEffectiveDt() {
			return EffectiveDt;
		}

		public void setEffectiveDt(String effectiveDt) {
			EffectiveDt = effectiveDt;
		}

	}
}
