package com.csc.rest.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;



@XmlAccessorType(XmlAccessType.NONE)
public class SearchCriteriaInfo implements Serializable {
	@XmlElement
	public String SearchCriteriaCd ;
	@XmlElement
	public int SearchCriteriaValue;

	public String getSearchCriteriaCd() {
		return SearchCriteriaCd;
	}

	
	public void setSearchCriteriaCd(String searchCriteriaCd) {
		SearchCriteriaCd = searchCriteriaCd;
	}

	public int getSearchCriteriaValue() {
		return SearchCriteriaValue;
	}

	
	public void setSearchCriteriaValue(int searchCriteriaValue) {
		SearchCriteriaValue = searchCriteriaValue;
	}

}
