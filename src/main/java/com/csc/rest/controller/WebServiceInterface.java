package com.csc.rest.controller;





import java.io.IOException;

import com.csc.rest.model.ResponseInfo;
import com.csc.rest.model.Search;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.xml.bind.JAXBException;

import org.apache.cxf.jaxrs.model.wadl.Description;
import org.apache.cxf.jaxrs.model.wadl.Descriptions;
import org.apache.cxf.jaxrs.model.wadl.DocTarget;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Path("/Search")
@WebService
@SOAPBinding(
	    style = SOAPBinding.Style.RPC,
	    use   = SOAPBinding.Use.LITERAL
	     )
public interface WebServiceInterface {
	@WebMethod
	@POST
	@Path("/Policy")
	@RequestMapping(value = "/Policy")
	@Produces({ "application/json","application/xml" })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	@Descriptions({
		@Description(value = "return Policies ", target = DocTarget.METHOD),
		@Description(value = "All the policy matching input criteria", target = DocTarget.RETURN)
	})
	String SearchPolicy(String info) throws JAXBException, IOException;
	//ResponseInfo[] SearchPolicy(Search info) throws JAXBException, IOException;
	@WebMethod
	@POST
	@Path("/PolicyDetails")
	@Produces({  "application/json","application/xml"  })
	@Consumes({ "application/xml", "application/json", "application/x-www-form-urlencoded" })
	@Descriptions({
		@Description(value = "returns details of one policy ", target = DocTarget.METHOD),
		@Description(value = "Policy Details", target = DocTarget.RETURN)
	})
	public String GetPolicyDetail( String info) throws JAXBException, IOException;
	//public <T> T[] GetPolicyDetail( ResponseInfo info) ;
}
