package com.csc.rest.controller;


import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jws.WebService;

import javax.ws.rs.PUT;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;



import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.Module;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jettison.json.JSONArray;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;





import com.csc.rest.model.*;
import com.csc.rest.model.CommlLine.CommPolicy;
import com.csc.rest.services.DataServices;

import com.thoughtworks.xstream.XStream;


@Service("WebServiceInterface")
@WebService(endpointInterface= "com.csc.rest.controller.WebServiceInterface")
public class RestController implements WebServiceInterface {
@Autowired
	DataServices dataServices;

	static final Logger logger = Logger.getLogger(RestController.class);

	

	public 
	String SearchPolicy(String data) throws JAXBException, IOException 
	{Search info;

		if(data.indexOf("{")!=-1 && data.indexOf(":")!=-1)
		{
	
		info=(Search)stringToObjectJSON(data);
		}
		else
		{
		 info=(Search)stringToObject(data);
		}
		List<ResponseInfo> result = new ArrayList<ResponseInfo>();

		try {
			logger.debug("Entering Search Service....");
			result = dataServices.pASearch(info);

		} catch (Exception e) {
			logger.error("Exception caught : ", e);
			e.printStackTrace();
		}
		logger.debug("Returning json Response....");
		ResponseInfo[] responseInfo = result.toArray(new ResponseInfo[result
				.size()]);
	
		String xml;
//		String xml = objectToXml(responseInfo);
	
		if(data.indexOf("{")!=-1 && data.indexOf(":")!=-1)
		{
		 xml=objectToJSON(responseInfo);
		}
		else
		{
			xml = objectToXml(responseInfo);
		}
		return xml;
		
	}
	public  String objectToXml(Object Response) {
		XStream xstream = new XStream();
		xstream.alias("Response", ResponseInfo.class);
		return xstream.toXML(Response);

	}
	public static String objectToXmlDetail(Object Response) {
		XStream xstream = new XStream();
		xstream.alias("Response", CommPolicy.class);
		return xstream.toXML(Response);
		

	}
	public Object stringToObject(String data) throws JAXBException, IOException 
	{
		JAXBContext jaxbContext = JAXBContext.newInstance(Search.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		StringReader read = new StringReader(data);
		Search info = (Search) unmarshaller.unmarshal(read);
		return info;
	}
	public Object stringToObjectdetail(String data) throws JAXBException, IOException 
	{
		JAXBContext jaxbContext = JAXBContext.newInstance(ResponseInfo.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		StringReader read = new StringReader(data);
		ResponseInfo info = (ResponseInfo) unmarshaller.unmarshal(read);
		return info;
	}
	
	
	
	@SuppressWarnings("unchecked")
	

	public
	String GetPolicyDetail(String data) throws JAXBException, IOException{
		ResponseInfo info;
		if(data.indexOf("{")!=-1 && data.indexOf(":")!=-1)
		{
	
		 info	=(ResponseInfo)stringToObjectdetailJSON(data);
		}
		else
		{
	 info	=(ResponseInfo)stringToObjectdetail(data);
		}
		
		List result = new ArrayList();
		Object Response = null;
		try {
			logger.debug("Entering Search Service....");

			result = (List) dataServices.GetCommlPolicyDetails(info);

		} catch (Exception e) {
			logger.error("Exception caught : ", e);
			e.printStackTrace();
		}

		logger.debug("Returning json Response....");
		if(info.persPolicy.getSymbol().equals("CPP"))
		{
		 Response =  result.toArray(new CommPolicy[result
				.size()]);
		}
		else if (info.persPolicy.getSymbol().equals("APV"))
		{
			Response=  result.toArray(new PolicyDetails[result.size()]);
		}
		String xml;
	
		if(data.indexOf("{")!=-1 && data.indexOf(":")!=-1)
		{
	
 xml = objectToXmlDetailJSON(Response);
		}
		else
		{
		 xml = objectToXmlDetail(Response);
		}




		return xml;

		

	}
	public  String objectToXmlDetailJSON(Object Response) {

	System.out.println(Response.toString());
CommPolicy[] cp=(CommPolicy[])Response;
//CommPolicy
Map<String, String> map1 = new HashMap<String, String>();
map1.put("PolicyNumber",cp[0].commlPolicy.getPolicyNumber());
map1.put("LOBCd", cp[0].commlPolicy.getLOBCd());
map1.put("NAICCd", cp[0].commlPolicy.getNAICCd());
map1.put("LOBCd", cp[0].commlPolicy.getLOBCd());
map1.put("ControllingStateProvCd", cp[0].commlPolicy.getControllingStateProvCd());

Map<String, JSONObject> contractTerm = new HashMap<String, JSONObject>();
Map<String, String> contractTermInside = new HashMap<String, String>();
contractTermInside.put("EffectiveDt", cp[0].commlPolicy.getContractTerm().getEffectiveDt());
JSONObject jsonContractTerm = new JSONObject();
jsonContractTerm.putAll(contractTermInside);
contractTerm.put("contractTerm", jsonContractTerm);

map1.put("OriginalInceptionDt", cp[0].commlPolicy.getOriginalInceptionDt());
map1.put("RateEffectiveDt", cp[0].commlPolicy.getRateEffectiveDt());
map1.put("PolicyTermCd", cp[0].commlPolicy.getPolicyTermCd());

JSONObject commlPolicyJson=new JSONObject();
commlPolicyJson.putAll(map1);
commlPolicyJson.putAll(contractTerm);

Map<String, JSONObject> commlPolicy = new HashMap<String, JSONObject>();
commlPolicy.put("commlPolicy",commlPolicyJson);

//Location
Map<String, String> map2 = new HashMap<String, String>();
map2.put("Id",cp[0].location.get(0).getId());

Map<String,JSONObject> addr=new HashMap<String, JSONObject>();
Map<String,String> PostalCode=new HashMap<String,String>();
PostalCode.put("PostalCode",cp[0].location.get(0).addr.getPostalCode());
JSONObject addrJSOnObj=new JSONObject();
addrJSOnObj.putAll(PostalCode);
addr.put("addr", addrJSOnObj);

JSONObject PDLoc=new JSONObject();
PDLoc.putAll(addr);
PDLoc.putAll(map2);

Map<String,JSONObject> mappdlo=new HashMap<String, JSONObject>();
mappdlo.put("com.csc.rest.model.PolicyDetails-Location", PDLoc);

JSONObject jo=new JSONObject();
jo.putAll(mappdlo);

Map<String,JSONObject> location=new HashMap<String, JSONObject>();
location.put("location",jo );
//commlPropertyLineBusiness

Map<String, String> map3 = new HashMap<String, String>(); 
map3.put("LOBCd", cp[0].commlPropertyLineBusiness.getLOBCd());

Map<String, String> map4= new HashMap<String, String>(); 
map4.put("Amt", cp[0].commlPropertyLineBusiness.currentTermAmt.getAmt());

JSONObject jj=new JSONObject();
jj.putAll(map4);

Map<String, JSONObject> cta = new HashMap<String, JSONObject>(); 
cta.put("currentTermAmt",jj);

JSONObject cplbjob=new JSONObject();
cplbjob.putAll(map3);
cplbjob.putAll(cta);

Map<String, JSONObject> CPLB = new HashMap<String, JSONObject>(); 
CPLB.put("commlPropertyLineBusiness",cplbjob );

//Response
JSONObject RResponse=new JSONObject();
RResponse.putAll(commlPolicy);
RResponse.putAll(location);
RResponse.putAll(CPLB);

Map<String, JSONObject> ResponseMap = new HashMap<String, JSONObject>(); 
ResponseMap.put("Response", RResponse);

//responseArray
JSONObject RAResponse=new JSONObject();
RAResponse.putAll(ResponseMap);
Map<String, JSONObject> ResponseArrayMap = new HashMap<String, JSONObject>(); 
ResponseArrayMap.put("Response-array", RAResponse);

JSONObject finalString=new JSONObject();
finalString.putAll(ResponseArrayMap);
		return finalString.toString();
	}
	
	
	public  String objectToJSON(Object Response) {
		
		
	ResponseInfo[] responseInfos=(ResponseInfo[])Response;
		

	JSONArray ja = new JSONArray();


		for(int i=0; i< responseInfos.length ;)
		{
Map<String, String> map1 = new HashMap<String, String>();

map1.put("policynum", responseInfos[i].persPolicy.getPolicynum());
map1.put("symbol", responseInfos[i].persPolicy.getSymbol());
map1.put("module", responseInfos[i].persPolicy.getModule());
map1.put("masterco", responseInfos[i].persPolicy.getMasterco());
map1.put("location", responseInfos[i].persPolicy.getLocation());


Map<String, String> map2 = new HashMap<String, String>();

map2.put("EffectiveDt", responseInfos[i].persPolicy.contractTerm.getEffectiveDt());
map2.put("ExpirationDt",responseInfos[i].persPolicy.contractTerm.getExpirationDt());

JSONObject jsonContractTerm = new JSONObject();
jsonContractTerm.putAll(map2);
Map<String, JSONObject> map = new HashMap<String, JSONObject>();
map.put("contractTerm", jsonContractTerm);

JSONObject mapperspolicy = new JSONObject();
	
mapperspolicy.putAll(map1);
mapperspolicy.putAll(map);
Map<String, JSONObject> mappers = new HashMap<String, JSONObject>();
mappers.put("persPolicy",mapperspolicy );

JSONObject FinalResponse=new JSONObject();
FinalResponse.putAll(mappers);


ja.put(FinalResponse);




i++;
		}
	
		
		Map mainObj = new HashMap();
		mainObj.put("\"Response\"", ja);
	
		
	
		Map Output =new HashMap();
	    Output.put("\"Response-array\"",mainObj );
	

   
	return Output.toString().replace("=", ":");
	

	}
	public static String objectToJSONDetail(Object Response) {
		XStream xstream = new XStream();
		xstream.alias("Response", CommPolicy.class);
		return xstream.toXML(Response);

	}
	public Object stringToObjectdetailJSON(String data) throws JAXBException, IOException 
	{
		String data1;
	//	Map Params = ConvertToParamMapDetail(data);
		Map Params = ConvertToParamMap(data,0);
		data=Params.get("responseInfo").toString();
		
	    Params = ConvertToParamMap(data,0);
		data=Params.get("persPolicy").toString();
	
		
		// Params = ConvertToParamMapDetail(data);
		 Params = ConvertToParamMap(data,2);
		 
			
			ResponseInfo ri=new ResponseInfo();
			if(Params.get("policynum")!=null)
			ri.persPolicy.setPolicynum(Params.get("policynum").toString());
			if(Params.get("masterco")!=null)
		ri.persPolicy.setMasterco(Params.get("masterco").toString());
			if(Params.get("module")!=null)
		ri.persPolicy.setModule(Params.get("module").toString());
			if(Params.get("location")!=null)
		ri.persPolicy.setLocation(Params.get("location").toString());
	if(Params.get("symbol")!=null)
		ri.persPolicy.setSymbol(Params.get("symbol").toString());
			
			
			//Manupalate data to change it to proper format which could be parsed
			data=Params.get("contractTerm").toString().replace("=", ":");
			
			
			String str=data;

			int i=str.indexOf(":")+11;
			str=str.substring(0, i).concat("\"")+str.substring(i,str.length());
			int j=str.lastIndexOf(":")+11;
			str=str.substring(0, j).concat("\"")+str.substring(j,str.length());
			
			str=str.replace(":", ":\"");
	
			data=str;
			Params = ConvertToParamMap(data,1);
		
		if(Params.get("EffectiveDt")!=null)
		ri.persPolicy.contractTerm.setEffectiveDt(Params.get("EffectiveDt").toString());
		if(Params.get("ExpirationDt")!=null)
		ri.persPolicy.contractTerm.setExpirationDt(Params.get("ExpirationDt").toString());
		
		
		return ri;
	}
	public Object stringToObjectJSON(String data) throws JAXBException, IOException 
	{
		String data1;
		Map Params = ConvertToParamMap(data,0);
		data=Params.get("search").toString();
		Params = ConvertToParamMap(data,0);
		data=Params.get("searchInfo").toString();
		Params = ConvertToParamMap(data,0);
		data=Params.get("partialPolicy").toString();
		data1=Params.get("searchCriteriaInfo").toString();
		
		Search info=new Search();
		Params = ConvertToParamMap(data,1);
		Params.get("CompanyProductCd").toString();
		if(Params.get("CompanyProductCd")!=null)
		info.searchInfo.partialPolicy.setCompanyProductCd(Params.get("CompanyProductCd").toString());
		if(Params.get("ContractTerm")!=null)
		info.searchInfo.partialPolicy.setContractTerm(Params.get("ContractTerm").toString());
		if(Params.get("ControllingStateProvCd")!=null)
		info.searchInfo.partialPolicy.setControllingStateProvCd(Params.get("ControllingStateProvCd").toString());
		if(Params.get("LOBCd")!=null)
		info.searchInfo.partialPolicy.setLOBCd(Params.get("LOBCd").toString());
		if(Params.get("NAICCd")!=null)
		info.searchInfo.partialPolicy.setNAICCd(Params.get("NAICCd").toString());

		Params = ConvertToParamMap(data1,1);
		if(Params.get("SearchCriteriaCd")!=null)
		info.searchInfo.searchCriteriaInfo.setSearchCriteriaCd(Params.get("SearchCriteriaCd").toString());
		if(Params.get("SearchCriteriaValue")!=null)
		info.searchInfo.searchCriteriaInfo.setSearchCriteriaValue(Integer.parseInt(Params.get("SearchCriteriaValue").toString()));
        return info;
	}

	private Map ConvertToParamMap(String jsonString,int strflag) {
		Map<String, JSONObject> map = new HashMap<String, JSONObject>();
		Map<String, String> map1 = new HashMap<String, String>();
		Map<String, Object> map2 = new  HashMap<String, Object>();
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(
				org.codehaus.jackson.JsonParser.Feature.ALLOW_SINGLE_QUOTES,
				true);
		mapper.configure(
				org.codehaus.jackson.JsonParser.Feature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER,
				true);
		mapper.configure(org.codehaus.jackson.JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES,true);
	
		try {
if(strflag==0)
{// convert JSON string to Map
			map = mapper.readValue(jsonString,
					new TypeReference<HashMap<String, JSONObject>>() {
			});
}
if(strflag==1)
{
	map1 =mapper.readValue(jsonString,
			new TypeReference<HashMap<String, String>>() {
	});
}
if(strflag==2)
{
	map2=mapper.readValue(jsonString,
			new TypeReference<HashMap<String, Object>>() {});
}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(strflag==0)
		return map;
		if(strflag==1)
			return map1;
		else
			return map2;
	}


}
